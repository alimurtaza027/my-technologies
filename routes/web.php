<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/dashboard',function(){
return view('admin.dashboard');
});
Route::get('/user/dashboard','PostController@userDashboard');
Route::resource('tag','TagController');
Route::resource('post','PostController');
Route::get('/delete/post/tag/{postId}/{tagId}','PostController@deletePostTag');
Route::get('/tag/posts/{tagId}','PostController@postsWithTag');
});