@extends('admin.partial.master')
@section('content')
	<section class="content-header">
	    <h1>
	        Dashboard
	    </h1>
	    <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li class="active">Dashboard</li>
	    </ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div>
			<h2><b>Name:: </b>Ali Murtaza</h2>
			<h2>Laravel developer</h2>
		</div>
		<div class="row">
		    <div class="col-lg-3 col-xs-6">
		        <!-- small box -->
		        <div class="small-box bg-aqua">
		            <div class="inner">
		                <h3>
		                   2 years
		                </h3>
		                <p>
		                    Total Experience
		                </p>
		            </div>
		            <a href="#" class="small-box-footer">
		                More info <i class="fa fa-arrow-circle-right"></i>
		            </a>
		        </div>
		    </div><!-- ./col -->
		    <div class="col-lg-3 col-xs-6">
		        <!-- small box -->
		        <div class="small-box bg-green">
		            <div class="inner">
		                <h3>
		                    1.2 years
		                </h3>
		                <p>
		                   Laravel
		                </p>
		            </div>
		            <div class="icon">
		                <i class="ion ion-stats-bars"></i>
		            </div>
		            <a href="#" class="small-box-footer">
		                More info <i class="fa fa-arrow-circle-right"></i>
		            </a>
		        </div>
		    </div><!-- ./col -->
		    <div class="col-lg-3 col-xs-6">
		        <!-- small box -->
		        <div class="small-box bg-yellow">
		            <div class="inner">
		                <h3>
		                    6 Month
		                </h3>
		                <p>
		                    Core Php 
		                </p>
		            </div>
		            <div class="icon">
		                <i class="ion ion-person-add"></i>
		            </div>
		            <a href="#" class="small-box-footer">
		                More info <i class="fa fa-arrow-circle-right"></i>
		            </a>
		        </div>
		    </div><!-- ./col -->
		    <div class="col-lg-3 col-xs-6">
		        <!-- small box -->
		        <div class="small-box bg-red">
		            <div class="inner">
		                <h3>
		                   6 Month
		                </h3>
		                <p>
		                    some other
		                </p>
		            </div>
		            <div class="icon">
		                <i class="ion ion-pie-graph"></i>
		            </div>
		            <a href="#" class="small-box-footer">
		                More info <i class="fa fa-arrow-circle-right"></i>
		            </a>
		        </div>
		    </div><!-- ./col -->
		</div><!-- /.row -->

		<!-- top row -->
		<div class="row">
		    <div class="col-xs-12 connectedSortable">
		        
		    </div><!-- /.col -->
		</div>
	</section><!-- /.content -->
@endsection