@extends('admin.partial.master')
@section('content')
<div class="row custom_row">
	<div class="col-md-12">
		<h1>Update A Tag</h1>
	</div>
	<div class="col-md-6">
		<form method="post" action="{{url('tag/'.$tag->id)}}">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="col-md-12">
				<label>Tag Nmae</label>
                <input type="test" name="tag" class="form-control" value="{{$tag->tag}}" required="ture">
			</div>
			<div class="col-md-12">
				<br>
				<input type="submit" value="UPDATE" class="btn btn-success" class="pull-right" >
			</div>
		</form>
	</div>
</div>
@endsection