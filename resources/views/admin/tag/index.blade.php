@extends('admin.partial.master')
@section('content')
<div class="col-md-12">
	<table id="tag">
		<tr>
			<th>Sr.</th>
			<th>Tag</th>
			<th>Show</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		@if(count($tags))
			@foreach($tags as $key=>$tag)
			<tr>
				<td>{{$key +1}}</td>
				<td>{{$tag->tag}}</td>
				<td><a href="{{url('/tag/'.$tag->id)}}">Show</a></td>
				<td><a href="{{url('/tag/'.$tag->id.'/edit')}}">Edit</a></td>
				<td>
					<form method="post" action="{{url('tag/'.$tag->id)}}">
						{{ csrf_field() }}
				        {{ method_field('DELETE') }}
						<input type="submit" value="Delete">
					</form>
			</tr>
			@endforeach
		@else
		   <p>There is no tag.</p>
		@endif	
	</table>
</div>
@endsection