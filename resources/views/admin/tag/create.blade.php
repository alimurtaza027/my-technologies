@extends('admin.partial.master')
@section('content')
<div class="row custom_row">
	<div class="col-md-12">
		<h1>Add A Tag</h1>
	</div>
	<div class="col-md-6">
		<form method="post" action="{{route('tag.store')}}">
			{{ csrf_field() }}
			<div class="col-md-12">
				<label>Tag Name</label>
				<input type="text" name="tag" class="form-control" required="ture">
			</div>
			<div class="col-md-12">
				<br>
				<input type="submit" value="ADD" class="btn btn-success" class="pull-right" >
			</div>
		</form>
	</div>
</div>
@endsection