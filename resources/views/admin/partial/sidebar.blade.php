<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/img/avatar3.png')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Ali Murtaza</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="{{url('/admin/dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('post.create')}}">
                    <i class="fa fa-th"></i> <span>Create Post</span> <small class="badge pull-right bg-green">new</small>
                </a>
            </li>
            <li>
                <a href="{{route('post.index')}}">
                    <i class="fa fa-th"></i> <span>All Post</span>
                </a>
            </li>
            <li>
                <a href="{{route('tag.create')}}">
                    <i class="fa fa-th"></i> <span>Create Tag</span> <small class="badge pull-right bg-green">new</small>
                </a>
            </li>
            <li>
                <a href="{{route('tag.index')}}">
                    <i class="fa fa-th"></i> <span>All Tag</span>
                </a>
            </li>
        </ul>
    </section>
                <!-- /.sidebar -->
</aside>