<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('admin.partial.css')
    </head>
    <body class="skin-black">
    	@include('admin.partial.header')
    	<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            @include('admin.partial.sidebar')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <div class="row" style="padding: 2%;">
            		@include('admin.partial.messages')
            		@yield('content')
            	</div>
                <!-- Content Header (Page header) -->
                
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        @include('admin.partial.js')
    </body>
</html>