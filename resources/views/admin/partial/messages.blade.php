@if (Session::has('success'))
	<div role="alert" class="alert alert-success fade in alert-dismissable" style="color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;">
	    <div class="icon"><span class="mdi mdi-check"></span></div>
		    <div class="message">
		      <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>Success!</strong> {{ Session::get('success') }}
		    </div>
	 </div>
@endif

@if (Session::has('danger'))
	<div role="alert" class="alert alert-danger alert-icon alert-dismissible">
        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
	        <div class="message">
	          <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>Danger!</strong> {{ Session::get('danger') }}
	        </div>
     </div>
@endif


@if (count($errors) > 0)

	<div class="alert alert-danger" role="alert">
		<strong>Errors:</strong>
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach  
		</ul>
	</div>

@endif


<script type="text/javascript">
	
</script>