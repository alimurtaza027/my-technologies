@extends('admin.partial.master')
@section('content')
<div class="col-md-12">
	<h1>All Posts</h1>
	<table id="tag">
		<tr>
			<th>Sr.</th>
			<th>Title</th>
			<th>Image</th>
			<th>Show</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		@if(count($posts))
			@foreach($posts as $key=>$post)
			<tr>
				<td>{{$key +1}}</td>
				<td>{{$post->title}}</td>
				<td><img src="{{asset('avatars/'.$post->avatar)}}" class="postImage"></td>
				<td><a href="{{url('/post/'.$post->id)}}">Show</a></td>
				<td><a href="{{url('/post/'.$post->id.'/edit')}}">Edit</a></td>
				<td>
					<form method="post" action="{{url('post/'.$post->id)}}">
						{{ csrf_field() }}
				        {{ method_field('DELETE') }}
						<input type="submit" value="Delete">
					</form>
			</tr>
			@endforeach
		@else
		<p>There is no post.</p>	
		@endif	
	</table>
</div>
@endsection