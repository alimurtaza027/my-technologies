@extends('admin.partial.master')
@section('content')
<div class="row custom_row">
	<div class="col-md-12">
		<h1>A Post Detail</h1>
	</div>
	<div class="col-md-6">
			<div class="col-md-12">
				<label>Post Title</label>
				<input type="text" name="title" class="form-control" value="{{$post->title}}" readonly="ture">
			</div>
			<div class="col-md-12">
				<label>Post Description</label>
				<textarea name="description" class="form-control" cols="10" rows="10" readonly="ture">{{$post->description}}</textarea>
			</div>
			<div class="col-md-12">
				<label>Post Image</label>
				<img width="200" src="{{asset('avatars/'.$post->avatar)}}">
			</div>
			<div class="col-md-12 ">
				<label>	Post Tags</label>
				<div class="tags">
					@foreach($post->tags as $tag)
                        <p class="tagedit">{{$tag->tag}}</p>
					@endforeach
				</div>
			</div>
			<div class="col-md-12">
				<br>
				<a href="{{ URL::previous() }}" class="btn btn-success">Back</a>
			</div>
		</form>
	</div>
</div>
@endsection