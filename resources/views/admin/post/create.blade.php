@extends('admin.partial.master')
@section('content')
<div class="row custom_row">
	<div class="col-md-12">
		<h1>Add A Post</h1>
	</div>
	<div class="col-md-6">
		<form method="post" action="{{route('post.store')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
				<label>Post Title</label>
				<input type="text" name="title" class="form-control" required="ture">
			</div>
			<div class="col-md-12">
				<label>Post Description</label>
				<textarea name="description" class="form-control" cols="10" rows="10"></textarea>
			</div>
			<div class="col-md-12">
				<label>Post Image</label>
				<input type="file" name="avatar" class="form-control" required="ture">
			</div>
			<div class="col-md-12 ">
				<label>Add Tags</label>
				<div class="tags">
					<select name="tags[]" class="form-control">
					<option value="">select</option>
					@foreach($tags as $tag)
					<option value="{{$tag->id}}">{{$tag->tag}}</option>
					@endforeach
				</select>
				</div>
				<br>
				<a href="javascript:void(0)" onclick="addTag()" class="btn btn-primary">Add Tag</a>
				
				
			</div>
			<div class="col-md-12">
				<br>
				<input type="submit" value="ADD POST" class="btn btn-success" class="pull-right" >
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	function addTag()
	{
		$('.tags').append('<select name="tags[]" class="form-control"><option>select</option>@foreach($tags as $tag)<option value="{{$tag->id}}">{{$tag->tag}}</option>@endforeach</select>');
	}
</script>
@endsection