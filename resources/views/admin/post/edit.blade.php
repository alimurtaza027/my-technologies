@extends('admin.partial.master')
@section('content')
<div class="row custom_row">
	<div class="col-md-12">
		<h1>Update A Post</h1>
	</div>
	<div class="col-md-6">
		<form method="post" action="{{url('post/'.$post->id)}}" enctype="multipart/form-data">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="col-md-12">
				<label>Post Title</label>
				<input type="text" name="title" class="form-control" value="{{$post->title}}">
			</div>
			<div class="col-md-12">
				<label>Post Description</label>
				<textarea name="description" class="form-control" cols="10" rows="10">{{$post->description}}</textarea>
			</div>
			<div class="col-md-12">
				<label>Post Image</label>
				<img width="200" src="{{asset('avatars/'.$post->avatar)}}">
			</div>
			<div class="col-md-12" id="newImage">
				<label>Post New Image</label>
				<input type="file" name="avatar" class="form-control" >
			</div>
			<div class="col-md-12 ">
				<label>	Post Tags</label>
				<div class="tags">
					<table id="tag">
						<tr>
							<th>Tags</th>
							<th>Action</th>
						</tr>
						@foreach($post->tags as $tag)
                         <tr>
							<td>{{$tag->tag}}</td>
							<td>
								<a href="{{url('/delete/post/tag/'.$post->id.'/'.$tag->id)}}" class="btn btn-danger">Delete</a>
							</td>
						</tr>
						@endforeach
					</table>
					
				</div>
				<br>
				<a href="javascript:void(0)" onclick="addTag()" class="btn btn-primary">Add Tag</a>
				
				
			</div>
			<div class="col-md-12">
				<br>
				<input type="submit" value="UPDATE POST" class="btn btn-success" class="pull-right" >
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#newImage').hide();
	function addTag()
	{
		$('.tags').append('<select name="newTags[]" class="form-control"><option>select</option>@foreach($tags as $tag)<option value="{{$tag->id}}">{{$tag->tag}}</option>@endforeach</select>');
	}
</script>
@endsection