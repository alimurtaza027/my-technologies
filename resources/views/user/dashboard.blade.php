@extends('user.master')
@section('content')
@if(count($posts))
	@foreach($posts as $key=>$post)
		<div class="col-md-12 postDiv">
			<div class="col-md-12">
				<p><b>Title ::</b>{{$post->title}}</p>
			</div>
			<div class="col-md-12">
		        <p><b>Description ::</b>{{$post->description}}</p>
			</div>
			<div class="col-md-12">
		        <p><b>Image</b></p>
		        <img src="{{asset('avatars/'.$post->avatar)}}" width="300">
			</div>
			<div class="col-md-12">
		        <p><b>Tags</b></p>
		        @foreach($post->tags as $tag)
	              <a href="{{url('/tag/posts/'.$tag->id)}}"><p class="tagedit">{{$tag->tag}}</p></a>
		        @endforeach
			</div>
		</div>
	@endforeach
@else
   <p>There is no post.</p>	
@endif
@endsection