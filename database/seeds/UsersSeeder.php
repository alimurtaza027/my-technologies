<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = ['admin', 'user'];
        foreach ($users as $user) {
            \App\User::create([
                'role_id' => \App\Role::where('name', $user)->pluck('id')[0],
                'name' => $user,
                'email' => $user.'@'.$user.'.com',
                'password' => Hash::make($user)
            ]);
        }
    }
}
