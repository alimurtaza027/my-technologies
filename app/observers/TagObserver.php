<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 27-Dec-17
 * Time: 6:43 PM
 */
namespace App\Observers;


class TagObserver
{
    public function deleting($tag)
    {
        if(count($tag->posts) > 0) $tag->posts()->detach();
    }

}