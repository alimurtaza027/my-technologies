<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 27-Dec-17
 * Time: 6:43 PM
 */
namespace App\Observers;


class PostObserver
{
    public function deleting($post)
    {
        if(count($post->tags) > 0) $post->tags()->detach();
    }

}