<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('admin.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags=Tag::all();
        return view('admin.post.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
         'title' => 'required',
         'description' => 'required',
         'avatar' => 'required'
        ]);
        if($request->file('avatar'))
        {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            $request->file('avatar')->move('avatars',$filename);
          
        }else{
            $filename='';
        }
        $post=Post::create([
            'title'      =>$request->title,
            'description'=>$request->description,
            'avatar'     =>$filename,

        ]);
        if(count($request->tags)){
            foreach ($request->tags as $key => $tag) {
                #if tag has no value
                if($tag !=null)
                if(in_array($tag,$post->tags()->pluck('tag_id')->toArray())==false){
                    #if tag is not already exist
                    $post->tags()->attach($tag);
                }
            }
        }
        Session::flash('success','Post has been successfully added.');
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id){
            $post = Post::findOrFail($id);
            $tags = Tag::all();
            return view('admin.post.edit',compact('post','tags'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        if($request->file('avatar')){
            $avatar = $request->file('avatar');
            #name the image with time and extention 
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            $request->file('avatar')->move('avatars',$filename);
            $post->avatar = $filename;
        }
        if($post){
            $post->title = $request->title;
            $post->description = $request->description;
        }
        $post->save();
        if(count($request->newTags)){
            foreach ($request->newTags as $key => $tag) {
                if($tag !=null)#if tag is empty or has no value
                if(in_array($tag,$post->tags()->pluck('tag_id')->toArray())==false){#if tag is not already exist
                    $post->tags()->attach($tag);
                }
            }
        }
        Session::flash('success','This post has successfully updated.');
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findorfail($id)->delete();
        Session::flash('success','This post has successfully deleted.');
        return redirect()->back();
    }

    public function deletePostTag($postId,$tagId)
    {
        $post = Post::findOrFail($postId);
        #remove all relationship value
        $post->tags()->detach($tagId);
        Session::flash('success','This post tag has successfully deleted.');
        return redirect()->back();
    }

    public function postsWithTag($tagId)
    {
        $tag = Tag::findOrFail($tagId);
        $posts = $tag->posts;
        return view('user.all-posts',compact('posts'));
    }
    public function userDashboard()
    {
        $posts = Post::all();
        return view('user.dashboard',compact('posts'));  
    }
}
