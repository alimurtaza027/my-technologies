<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminId = Role::where('name','admin')->first()->id;
        if(Auth::user()->role_id == $adminId){
            return redirect('/admin/dashboard');
        }
        return redirect('/user/dashboard');
    }
}
